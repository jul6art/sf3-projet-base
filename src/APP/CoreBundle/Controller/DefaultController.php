<?php

namespace APP\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('APPCoreBundle:Default:index.html.twig');
    }
}
