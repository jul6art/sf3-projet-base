<?php

namespace APP\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class UserAvatarType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->remove('nom')
            ->remove('prenom')
            ->remove('dateNaissance')
            ->remove('adresse')
            ->remove('suite')
            ->remove('codePostal')
            ->remove('localite')
            ->add('avatar', 'file', array('required'=>true,'attr'=>array('class'=>'form-control', 'accept'=>'image/png, image/jpeg, image/gif')))
        ;
    }
    
    public function getName()
  {
    return 'app_userbundle_user_avatar';
  }

  public function getParent()
  {
    return new UserType();
  }
}
